import dto.Electrodomestico;
import dto.Lavadora;
import dto.Television;

public class ElectrodomesticoApp {

	public static void main(String[] args) {
		Electrodomestico electrodomesticos[] = new Electrodomestico[10];//CREAMOS ARRAY DE ELECTRODOMESTICOS
		
		//CREAMOS OBJETOS
		Electrodomestico e1 = new Electrodomestico(100, "Blanco", 'A', 40);
		Electrodomestico e2 = new Electrodomestico(120, "Negro", 'C', 70);
		Electrodomestico e3 = new Electrodomestico(90, "Rojo", 'D', 80);
		Electrodomestico e4 = new Electrodomestico(105, "dsadasa", 'K', 100);
		
		
		Lavadora l1 = new Lavadora(140, "Blanco", 'A', 70, 40);
		Lavadora l2 = new Lavadora(200, "Amarillo", 'D', 120, 70);
		Lavadora l3 = new Lavadora(140, "Rojo", 'A', 100, 20);


		
		Television t1 = new Television(200, "Negro", 'A', 50, 25, false);
		Television t2 = new Television(400, "Blanco", 'B', 45, 22, false);
		Television t3 = new Television(320, "Rojo", 'K', 60, 45, true);

		//LOS AGREGAMOS AL ARRAY
		electrodomesticos[0] = e1;
		electrodomesticos[1] = e2;
		electrodomesticos[2] = e3;
		electrodomesticos[3] = e4;
		electrodomesticos[4] = l1;
		electrodomesticos[5] = l2;
		electrodomesticos[6] = l3;
		electrodomesticos[7] = t1;
		electrodomesticos[8] = t2;
		electrodomesticos[9] = t3;
		
		
		double precioFinal = 0;
		//RECORREMOS EL ARRAY
		for (int i = 0; i < electrodomesticos.length; i++) {
			//COMPROBAMOS QUE LA LETRA DEL CONSUMO ELECTRICO SEA CORRECTA
			electrodomesticos[i].comprobarConsumoEnergetico(electrodomesticos[i].getConsumoElectrico());
			//COMPROBAMOS QUE EL COLOR SEA CORRECTO
			electrodomesticos[i].comprovarColor(electrodomesticos[i].getColor());
			//MOSTRAMOS EL PRECIO FINAL DEL ELECTRODOMESTICO
			System.out.println("PRECIO: "+electrodomesticos[i].precioFinal());
			precioFinal += electrodomesticos[i].precioFinal();//SE LO SUMAMOS AL PRECIO TOTAL
			
		}
		System.out.println("PRECIO TOTAL: "+precioFinal);//MOSTRAMOS PRECIO TOTAL
		System.out.println(e4);




		
	}

}
