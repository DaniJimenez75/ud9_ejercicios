package dto;

public class Electrodomestico {

	
	protected final int precioB= 100;
	protected final String c = "blanco";
	protected final char consumo = 'F';
	protected final double p = 5;
	
	protected int precioBase;
	protected String color;
	protected char consumoElectrico;
	protected double peso;
	
	//CONSTRUCTOR POR DEFECTO
	public Electrodomestico() {
		super();
		this.precioBase = this.precioB;
		this.color=this.c;
		this.consumoElectrico = this.consumo;
		this.peso = this.p;
				
	}
	
	
	//Un constructor con el precio y peso. El resto por defecto
	public Electrodomestico(int precioBase, double peso) {
		super();
		this.precioBase = precioBase;
		this.color=this.c;
		this.consumoElectrico = this.consumo;
		this.peso = peso;
	}



	//Un constructor con todos los atributos
	public Electrodomestico(int precioBase, String color, char consumoElectrico, double peso) {
		super();
		this.precioBase = precioBase;
		this.color = color;
		this.consumoElectrico = consumoElectrico;
		this.peso = peso;
	}


	//GETTERS
	public int getPrecioBase() {
		return precioBase;
	}




	public String getColor() {
		return color;
	}




	public char getConsumoElectrico() {
		return consumoElectrico;
	}




	public double getPeso() {
		return peso;
	}
	
	/*
	 * Comprueba que la letra es correcta, sino es correcta usara la letra por defecto. 
	 * Se invocara al crear el objeto y no sera visible.
	 * 
	 */
	public void comprobarConsumoEnergetico(char letra) {
		
		if(letra != 'A' ||letra != 'B' ||letra != 'C' ||letra != 'D' ||letra != 'E' ||letra != 'F') {
			this.consumoElectrico = this.consumo;

		}
		
	}
	
	//Comprueba que el color es correcto, sino lo es usa el color 
	//por defecto. Se invocara al crear el objeto y no seravisible.
	public void comprovarColor(String color) {		
		if(color != "blanco" ||color != "BLANCO" ||color != "negro" ||color != "NEGRO" ||color != "rojo" ||color != "ROJO" ||
				color != "azul" ||color != "AZUL" ||color != "gris" ||color != "GRIS") {
			
			this.color = this.c;

		}
		
				
		
	}
	
	
	//Seg�n el consumo energ�tico, aumentara su precio, y seg�n su tama�o, tambi�n.
	public double precioFinal() {
		double a = 100;
		double b = 80;
		double c= 60;
		double d = 50;
		double e = 30;
		double f = 10;
		double suma = 0;
		
		//DEPENDIENDO DE LA LETRA LE SUMARA UN VALOR O OTRO
		switch (this.getConsumoElectrico()) {
		case 'A':
			suma = suma + a;
			break;
		case 'B':
			suma = suma + b;

			break;
		case 'C':
			suma = suma + c;

			break;
		case 'D':
			suma = suma + d;

			break;
		case 'E':
			suma = suma + e;

			break;
		case 'F':
			suma = suma + f;

			break;

		default:
			break;
		}
		
		

		double peso = this.getPeso();//OBTENEMOS EL PESO
		
		if(peso > 0 && peso<=19) {
			suma = suma + 10;
		}else if(peso >19 && peso <=49) {
			suma = suma + 50;
		}else if(peso >49 && peso <=79) {
			suma = suma + 80;
		}else if(peso > 79) {
			suma = suma + 100;
		}
		
		double precioFinal = this.getPrecioBase() + suma;//LO AGREGAMOS AL PRECIO FINAL

			return precioFinal;
	}


	@Override
	public String toString() {
		return "Electrodomestico [precioBase=" + precioBase + ", color=" + color + ", consumoElectrico="
				+ consumoElectrico + ", peso=" + peso + "]";
	}







	
}
