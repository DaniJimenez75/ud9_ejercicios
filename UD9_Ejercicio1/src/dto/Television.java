package dto;

public class Television extends Electrodomestico{

	protected int pulgadas;
	protected boolean sintonizadorTDT;
	
	//CONSTRUCTOR POR DEFECTO
	public Television() {
		this.pulgadas = 20;
		this.sintonizadorTDT = false;
		
	}
	
	//CONSTRUCTOR CON PRECIO Y PESO
	public Television(int precio, double peso) {
		super(precio, peso);
		this.pulgadas = 20;
		this.sintonizadorTDT = false;

	}

	//CONSTRUCTOR CON TODOS LOS PARAMETROS HEREDADOS, PULGADAS Y TDT
	public Television(int precioBase, String color, char consumoElectrico, double peso, int pulgadas, boolean sintonizadorTDT) {
		super(precioBase, color, consumoElectrico, peso);
		this.pulgadas = pulgadas;
		this.sintonizadorTDT = sintonizadorTDT;
	}

	//GETTERS
	public int getPulgadas() {
		return pulgadas;
	}

	public boolean isSintonizadorTDT() {
		return sintonizadorTDT;
	}
	
	
	@Override
	public double precioFinal() {
		double precioFinal = super.precioFinal();//RECOGEMOS EL PRECIO FINAL DE LA CLASE PADRE
				
		if(this.getPulgadas() > 40) { //Si tiene mas de 40 pulgadas
			double incremento = precioFinal * 0.30;
			precioFinal = precioFinal + incremento;
		}
		
		if(this.isSintonizadorTDT()) { //Si tiene TDT
			precioFinal = precioFinal + 50;

		}
		return precioFinal;
	}
	
}
