package dto;

public class Lavadora extends Electrodomestico{
	
	protected final int c = 5;

	protected int carga;
	
	//CONSTRUCTOR POR DEFECTO
	public Lavadora() {
		super();
		this.carga = c;
	}
	
	//CONSTRUCTOR CON PRECIO Y PESO
	public Lavadora(int precio, double peso) {
		super(precio, peso);
		this.carga = c;
	}

	//CONSTRUCTOR CON TODOS LOS PARAMETROS HEREDADOS Y LA CARGA
	public Lavadora(int precioBase, String color, char consumoElectrico, double peso, int carga) {
		super(precioBase, color, consumoElectrico, peso);
		this.carga = carga;
	}

	//GETTERS
	public int getCarga() {
		return carga;
	}

	@Override
	public double precioFinal() {
		double precioFinal = super.precioFinal();//RECOGEMOS EL PRECIO FINAL DE LA CLASE PADRE
		
		if(this.getCarga() > 30) {//Si la carga es superior a 30
			precioFinal = precioFinal + 50;

		}
		return precioFinal;
	}

	@Override
	public String toString() {
		return "Lavadora [carga=" + carga + ", precioBase=" + precioBase + ", color=" + color + ", consumoElectrico="
				+ consumoElectrico + ", peso=" + peso + "]";
	}

	
	
	
	
	

	

	
	
	
	
	
}
