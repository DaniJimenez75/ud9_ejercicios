/**
 * 
 */
import dto.Aula;
import dto.Estudiante;
import dto.Profesor;


/**
 * @author Dani
 *
 */
public class AulaApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//CREAMOS LOS ESTUDIANTES
		Estudiante e1 = new Estudiante("Alargo", 24, "Hombre", 8, 40);
		Estudiante e2 = new Estudiante("Dani", 20, "Hombre", 8, 35);
		Estudiante e3 = new Estudiante("Javi", 18, "Hombre", 3, 55);
		Estudiante e4 = new Estudiante("Laura", 30, "Mujer", 6, 1);
		Estudiante e5 = new Estudiante("Andreu", 23, "Hombre", 4, 1);
		Estudiante e6 = new Estudiante("Eric", 18, "Hombre", 7, 1);
		Estudiante e7 = new Estudiante("Adria", 19, "Hombre", 1, 1);
		Estudiante e8 = new Estudiante("Matias", 45, "Hombre", 9, 40);
		Estudiante e9 = new Estudiante("Guillem", 25, "Hombre", 5, 70);
		Estudiante e10 = new Estudiante("Cristina", 20, "Mujer", 4, 20);
		
		Estudiante[] e = {e1, e2, e3, e4, e5, e6, e7, e8, e9, e10}; //LOS METEMOS DENTRO DE UN ARRAY
		
		
		
		Profesor p1 = new Profesor("Ramon", 40, "Hombre", "filosofia", 5); //CREAMOS PROFESOR
		
		Aula a1 = new Aula(1, 15, "filosofia", p1, e); //CREAMOS AULA
		
		//MOSTRAMOS LOS DATOS POR PANTALLA
		System.out.println();
		System.out.println(p1);
		System.out.println();
		System.out.println(a1);

	}

}
