
package dto;


public class Estudiante extends Personas {
	
	protected double calificacion;
	protected int asistencia;
	protected boolean disponible;

	//CONSTRUCTOR
	public Estudiante(String nombre, int edad, String sexo, double calificacion, int asistencia) {
		super(nombre,edad,sexo);
		this.calificacion = calificacion;
		this.asistencia = asistencia;
		this.disponible = asistencia();
	}

	//GETTERS
	public int getAsistencia() {
		return asistencia;
	}

	public double getCalificacion() {
		return calificacion;
	}
		

	public boolean isDisponible() {
		return disponible;
	}

	
	public boolean asistencia() {
		boolean disponible = true;
		
		if(this.asistencia > 50) {//Si se pasa de faltas de asistencia devuelve false 
			disponible = false;
		}
		return disponible;
	}

	@Override
	public String toString() {
		return "Estudiante [calificacion=" + calificacion + ", asistencia=" + asistencia + ", disponible=" + disponible
				+ "]";
	}
}


