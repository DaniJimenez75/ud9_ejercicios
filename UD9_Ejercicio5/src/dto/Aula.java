
package dto;


public class Aula {
	protected int identificador;
	protected int numMaxEstudiantes;
	protected String Asignatura;
	protected Profesor p;
	protected Estudiante[] e;

	
	
	public Aula(int identificador, int numMaxEstudiantes, String asignatura,  Profesor p, Estudiante[] e) {
		this.identificador = identificador;
		this.numMaxEstudiantes = numMaxEstudiantes;
		this.Asignatura = asignatura;
		this.p = p;
		this.e = e;
		hacerClase(e, p);//LLAMAMOS AL METODO "hacerClase"
	}

	//GETTERS
	public int getIdentificador() {
		return identificador;
	}


	public int getNumMaxEstudiantes() {
		return numMaxEstudiantes;
	}


	public String getAsignatura() {
		return Asignatura;
	}

	
	//METODO PARA COMPROBAR QUE TODOS LOS METODOS ANTERIORES SON TRUE Y SE PUEDE DAR CLASE
	public void hacerClase(Estudiante[] e, Profesor p) {
		
		boolean comprobarAsignaturaProfe = comprobarAsignaturaProfe(p.getMateria());
		boolean comprobarAlumnos = comprobarAlumnos(e);
		boolean comprobarProfesor = comprobarProfesor(p);
		boolean nMaximoEstudiantes = numeroMaximoEstudiantes(e);
		
		//SI TODOS SON TRUE SE PUEDE DAR CLASE
		if(comprobarAlumnos && comprobarAsignaturaProfe && comprobarProfesor && nMaximoEstudiantes) {
			System.out.println("Se puede dar clase");
			alumnosAprobados(e);//MOSTRAMOS LOS ALUMNOS APROBADOS
		}else {
			System.out.println("No se puede dar clase");
		}
	}
	
	//METODO PARA COMPROBAR QUE LA MATERIA QUE DA EL PROFESOR ES IGUAL QUE LA DEL AULA
	public boolean comprobarAsignaturaProfe(String materia) {
		boolean puedeDarClase = false;
		 if(this.getAsignatura().equals(materia)) {
			 puedeDarClase = true;
		 }else {
			 System.out.println("La asignatura del profesor no es compatible con la del Aula");
		 }
		 return puedeDarClase;
	}
	
	//METODO QUE COMPRUEBA LOS ALUMNOS DISPONIBLES Y MIRAMOS SI HAY ALUMNOS SUFICIENTES PARA DAR CLASE
	public boolean comprobarAlumnos(Estudiante[] e) {
		boolean alumnosSuficientes = false;
		int contador = 0;
		for (int i = 0; i < e.length; i++) {
			if(e[i].isDisponible()) {//CONTAMOS LOS ALUMNOS DISPONIBLES
				contador++;
			}
		}
		
		int mitadEstudiantes = e.length / 2;//MIRAMOS CUAL ES LA MITAD DE TODOS LOS ALUMNOS REGISTRADOS
		if(contador >= mitadEstudiantes) {//SI LOS ALUMNOS DISPONIBLES ES MAS GRANDE O IGUAL A LA MITAD DE LOS ALUMNOS DEVUELVE TRUE
			alumnosSuficientes = true;
		}else {
			System.out.println("No hay alumnos suficientes para dar clase");
		}
		return alumnosSuficientes;
	}
	
	//METODO QUE COMPRUEBA SI EL PROFESOR ESTA DISPONIBLE
	public boolean comprobarProfesor(Profesor p) {
		boolean profesorDisponible = false;
		
		if(p.isDisponible()) {
			profesorDisponible = true;
		}else {
			System.out.println("El profesor no esta disponible");
		}
		
		return profesorDisponible;
	}
	
	//MUESTRA LOS ALUMNOS APROBADOS
	public void alumnosAprobados(Estudiante[] e) {
		System.out.println();
		System.out.println("-------------ALUMNOS APROBADOS-----------");
		for (int i = 0; i < e.length; i++) {
			if(e[i].getCalificacion() >= 5) {//MUESTRA LOS ALUMNOS QUE TIENEN UNA CALIFICACI�N MAS GRANDE O IGUAL A 5
				System.out.println(e[i]);
			}
		}
	}
	
	//METODO QUE COMPRUEBA QUE TODOS LOS ALUMNOS REGISTRADOS CABEN EN EL AULA
	public boolean numeroMaximoEstudiantes(Estudiante[] e) {
		boolean capacidad = false;
		if(e.length <= this.getNumMaxEstudiantes()) {
			capacidad = true;
		}else {
			System.out.println("El aula es demasiado peque�a para todos los alumnos registrados");
		}
		return capacidad;
	}

	@Override
	public String toString() {
		return "Aula [identificador=" + identificador + ", numMaxEstudiantes=" + numMaxEstudiantes + ", Asignatura="
				+ Asignatura + "]";
	}

	
	
	
	
	
	
	
	
	
	
}
