package dto;

public abstract class Personas { //Definimos que es una clase abstracta
	
	protected String nombre;
	protected int edad;
	protected String sexo;
	
	//Constructor por defecto
	public Personas() {

	}
	
	
	//Constructor con parametros
	public Personas(String nombre, int edad, String sexo) {
		this.nombre = nombre;
		this.edad = edad;
		this.sexo = sexo;
	}



	//GETTERS
	public String getNombre() {
		return nombre;
	}



	public int getEdad() {
		return edad;
	}

	

	public String getSexo() {
		return sexo;
	}


	//Creamos el metodo asistencia
	public abstract boolean asistencia();




	@Override
	public String toString() {
		return "Personas [nombre=" + nombre + ", edad=" + edad + ", sexo=" + sexo + "]";
	}
		
	
	
	
	
	

}
