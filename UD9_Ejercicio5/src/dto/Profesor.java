
package dto;


public class Profesor extends Personas {
	protected String materia;
	protected int asistencia;
	protected boolean disponible;

	
	//Constructor
	public Profesor(String nombre, int edad, String sexo, String materia, int asistencia) {
		super(nombre, edad, sexo);
		this.materia = materia;
		this.asistencia = asistencia;
		this.disponible = asistencia();

		
	}
	
	
	//GETTERS
	public boolean isDisponible() {
		return disponible;
	}



	public String getMateria() {
		return materia;
	}

	public int getAsistencia() {
		return asistencia;
	}
	
	//Definimos el metodo asistencia
	public  boolean asistencia() {
		boolean disponible = true;
		
		if(this.asistencia > 20) {//Si se pasa de faltas de asistencia devuelve false 
			disponible = false;
		}
		return disponible;
	}



	@Override
	public String toString() {
		return "Profesor [materia=" + materia + ", asistencia=" + asistencia + ", disponible=" + disponible + "]";
	}
	
	
	
	
	
	



	
	


	
	
	

	
	
	
	
	
}
