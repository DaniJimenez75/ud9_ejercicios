import dto.Serie;
import dto.VideoJuego;

public class SerieApp {

	public static void main(String[] args) {
		Serie series[] = new Serie[5];
		VideoJuego video[] = new VideoJuego[5];

		//Creamos el array y definimos las series y videojuegos
		Serie s1 = new Serie("TLOU",1,"Suspenso","Netflix");
		Serie s2 = new Serie("HIMYM",9,"Comedia","Pamela Fryman");
		Serie s3 = new Serie("TWD",10,"Suspenso","Greg Nicotero");
		Serie s4 = new Serie("Friends",8,"Comedia","Marta Kauffman ");
		Serie s5 = new Serie("Flash",5,"Accion","Glen Winter");
		
		VideoJuego v1 = new VideoJuego("TLOU",24,"Suspenso","Nauthy Dog");
		VideoJuego v2 = new VideoJuego("RDR2",30,"Accion","Rock Star");
		VideoJuego v3 = new VideoJuego("COD",100,"Accion","Activision");
		VideoJuego v4 = new VideoJuego("Battlefield",60,"Accion","EA DICE");
		VideoJuego v5 = new VideoJuego("LOL",200,"Rol","Riot Games");
		
		//Asignamos las series y videojuegos a los arrays
		series[0] = s1;
		series[1] = s2;
		series[2] = s3;
		series[3] = s4;
		series[4] = s5;
		
		video[0] = v1;
		video[1] = v2;
		video[2] = v3;
		video[3] = v4;
		video[4] = v5;
		
		video[0].entregar();
		video[1].entregar();
		video[2].entregar();
		
		series[0].entregar();
		series[1].entregar();
		series[2].entregar();
		
		int contadorv = 0;
		int contadors = 0;
		
		//Se hace un bucle para recorrer las entregas y contar las que quedan y las devuelva
		for (int i = 0; i < video.length; i++) {
			if (video[i].isEntregado()) {
				contadorv++;
				video[i].devolver();
			}
			System.out.println("El numero de videojuegos entregados " + contadorv);
		}
		
		for (int i = 0; i < series.length; i++) {
			if (series[i].isEntregado()) {
				contadors++;
				series[i].devolver();
			}
			System.out.println("El numero de series entregados " + contadors);
		}
		
		
		//se hace un bucle donde vamos a buscar dentro del array la posicion de las paginas y devolver el mayor
		int masGrande = 0;
		int posicion = 0;
		for (int i = 0; i < video.length; i++) {
			if (video[i].getHorasEstimadas() > masGrande) {
				masGrande = video[i].getHorasEstimadas();
				posicion = i;
			}
		}
		System.out.println("El videoJuego que tiene mas horas estimadas " + video[posicion]);
		
		 masGrande = 0;
		 posicion = 0;
		for (int i = 0; i < series.length; i++) {
			if (series[i].getNumeroTemporada() > masGrande) {
				masGrande = series[i].getNumeroTemporada();
				posicion = i;
			}
		}
		System.out.println("La serie que tiene mas temporadas estimadas " + series[posicion]);
		
	}

}
