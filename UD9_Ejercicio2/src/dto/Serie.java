package dto;

import Service.IEntregable;

public class Serie implements IEntregable {

	protected String titulo;
	protected int numeroTemporada;
	protected boolean entregado;
	protected String genero;
	protected String creador;
	
	protected final boolean entregadoFinal = false;
	protected final int temporadaFinal = 3;
	
	//Se crean las variables y los constructores para cada caso 
	
	public Serie() {
		this.titulo = "";
		this.numeroTemporada = temporadaFinal;
		this.entregado = entregadoFinal;
		this.genero = "";
		this.creador = "";
	}
	
	public Serie(String titulo, String creador) {
		this.titulo = titulo;
		this.numeroTemporada = temporadaFinal;
		this.entregado = entregadoFinal;
		this.genero = "";
		this.creador = creador;
	}
	
	public Serie(String titulo, int numeroTemporada, String genero, String creador) {
		this.titulo = titulo;
		this.numeroTemporada = numeroTemporada;
		this.genero = genero;
		this.creador = creador;
		this.entregado = entregadoFinal;
	}
	
	

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getNumeroTemporada() {
		return numeroTemporada;
	}

	public void setNumeroTemporada(int numeroTemporada) {
		this.numeroTemporada = numeroTemporada;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getCreador() {
		return creador;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	@Override
	public String toString() {
		return "Serie [titulo=" + titulo + ", numeroTemporada=" + numeroTemporada + ", entregado=" + entregado
				+ ", genero=" + genero + ", creador=" + creador + "]";
	}
	
	//se llama al interface con los metodos que se han creado, luego se desarrolan los metodos para implementar el main
	
	public void entregar() {
		entregado = true;
	}
	
	public void devolver() {
		entregado = false;
	}
	
	public boolean isEntregado() {
		return entregado;
	}
	
	public void compareTo(Serie serie) {
		if (this.numeroTemporada == serie.numeroTemporada) {
			System.out.println("Tienes el mismo numero de temporadas");
		}else if (this.numeroTemporada > serie.numeroTemporada) {
			System.out.println("Tiene menos temporadas");
		}else if (this.numeroTemporada < serie.numeroTemporada) {
			System.out.println("Tiene mas temporadas");
		}
	}
	
	
}
