package dto;

import Service.IEntregable;

public class VideoJuego implements IEntregable{
	protected String titulo;
	protected int horasEstimadas;
	protected boolean entregado;
	protected String genero;
	protected String compa�ia;
	
	protected final int horas = 10;
	protected final boolean entregadoFinal = false;
	
	//Se crean las variables y los constructores para cada caso
	
	public VideoJuego() {
		super();
		this.titulo = "";
		this.horasEstimadas = horas;
		this.entregado = entregadoFinal;
		this.genero = "";
		this.compa�ia = "";
	}

	public VideoJuego(String titulo, int horasEstimadas) {
		super();
		this.titulo = titulo;
		this.horasEstimadas = horasEstimadas;
		this.entregado = entregadoFinal;
		this.genero = "";
		this.compa�ia = "";
	}

	public VideoJuego(String titulo, int horasEstimadas,String genero, String compa�ia) {
		super();
		this.titulo = titulo;
		this.horasEstimadas = horasEstimadas;
		this.entregado = entregadoFinal;
		this.genero = genero;
		this.compa�ia = compa�ia;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getHorasEstimadas() {
		return horasEstimadas;
	}

	public void setHorasEstimadas(int horasEstimadas) {
		this.horasEstimadas = horasEstimadas;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getCompa�ia() {
		return compa�ia;
	}

	public void setCompa�ia(String compa�ia) {
		this.compa�ia = compa�ia;
	}

	@Override
	public String toString() {
		return "VideoJuego [titulo=" + titulo + ", horasEstimadas=" + horasEstimadas + ", entregado=" + entregado
				+ ", genero=" + genero + ", compa�ia=" + compa�ia + "]";
	}
	
	//se llama al interface con los metodos que se han creado, luego se desarrolan los metodos para implementar el main
	
	
	public void entregar() {
		entregado = true;
	}
	
	public void devolver() {
		entregado = false;
	}
	
	public boolean isEntregado() {
		return entregado;
	}
	
	public void compareTo(VideoJuego video) {
		if (this.horasEstimadas == video.horasEstimadas) {
			System.out.println("Tiene las mismas horas de juego");
		}else if (this.horasEstimadas > video.horasEstimadas) {
			System.out.println("Tiene menos horas de juego");
		}else if (this.horasEstimadas < video.horasEstimadas) {
			System.out.println("Tiene mas horas de juego");
		}
	}
	
	
	
	
	
	

}
