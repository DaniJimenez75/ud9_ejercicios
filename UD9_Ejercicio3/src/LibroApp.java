import dto.Libro;

public class LibroApp {

	public static void main(String[] args) {
		
		Libro l1 = new Libro(978148293,"Cien a�os de soledad", "Gabriel Garc�a M�rquez",471);
		Libro l2 = new Libro(972536541,"Odisea", "Homero",448);
		
		//se llaman los metodos y se crea   un array para almacenar los libros
		System.out.println(l1);
		System.out.println(l2);
		
		//Con un bucle se llama al numero de paginas y se compara cual tiene un numero mayor de paginas
		
		if (l1.getNumPaginas()>l2.getNumPaginas()) {
			System.out.println(l1.getTitulo() + " tiene mas paginas");
		}else if(l1.getNumPaginas()<l2.getNumPaginas())
			System.out.println(l2.getTitulo() + " tiene mas paginas");
	}

}
