package dto;

public class Libro {

	protected int ISBN;
	protected String titulo;
	protected String autor;
	protected int NumPaginas;
	
	//Se crean las variables y los metodos de retorno de cada variable
	public int getISBN() {
		return ISBN;
	}
	public void setISBN(int iSBN) {
		ISBN = iSBN;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public int getNumPaginas() {
		return NumPaginas;
	}
	public void setNumPaginas(int numPaginas) {
		NumPaginas = numPaginas;
	}
	
	public Libro(int iSBN, String titulo, String autor, int numPaginas) {
		super();
		ISBN = iSBN;
		this.titulo = titulo;
		this.autor = autor;
		NumPaginas = numPaginas;
	}
	@Override
	public String toString() {
		return  titulo + " con ISBN " + ISBN + "creado por el autor " + autor + " tiene " + NumPaginas + " paginas";
	}
	
	
	
	
	
	
	
}
