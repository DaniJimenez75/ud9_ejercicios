package dto;

public class Raices {
	protected int a;
	protected int b;
	protected int c;
	
	
	
	public Raices(int a, int b, int c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public void obtenerRaices() {
		double resultado1 = -this.b+Math.sqrt(Math.pow(this.b, 2)-(4*this.a*this.c)) /(2*this.a);
		System.out.println("Resultado 1: "+resultado1);
		double resultado2 = -this.b-Math.sqrt(Math.pow(this.b, 2)-(4*this.a*this.c)) /(2*this.a);
		System.out.println("Resultado 1: "+resultado2);

	}
	
	public void obtenerRaiz() {
		double resultado = -this.b+Math.sqrt(Math.pow(this.b, 2)-(4*this.a*this.c)) /(2*this.a);
		System.out.println("Resultado 1: "+resultado);
	}
	
	public double getDiscriminante() {
		double discriminante = (Math.pow(this.b, 2))-4*this.a*this.c;
		return discriminante;
	}
	
	public boolean tieneRaices() {
		boolean dosSoluciones = false;
		if(this.getDiscriminante() >= 0) {
			dosSoluciones = true;
		}
		
		return dosSoluciones;
	}
	
	public boolean tieneRaiz() {
		boolean unaSolucion = false;
		if(this.getDiscriminante() == 0) {
			unaSolucion = true;
		}
		
		return unaSolucion;
	}
	
	public void calcular() {
		if(this.tieneRaices()) {//Si tiene varias soluciones muestra las 2 posibles soluciones
			this.obtenerRaices();
		}else if(this.tieneRaiz()) {//Si no comprueba si tiene una unica y la muestra
			this.obtenerRaiz();
		}else {//Si ninguna de las dem�s no se cumple no tiene soluci�n
			System.out.println("NO TIENE SOLUCI�N");
		}
	}
	
	

	
}
