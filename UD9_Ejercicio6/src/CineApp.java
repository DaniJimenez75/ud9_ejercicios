import java.util.Random;

import dto.Cine;
import dto.Espectador;
import dto.Peliculas;

public class CineApp {

	public static void main(String[] args) {
		Random r1 = new Random();
		Peliculas p1 = new Peliculas("Avatar", 135, 16, "James Cameron");		
		Cine cine = new Cine(p1.getTitulo(), 8);
						
		Espectador espectadores[] = new Espectador[13];		
		rellenarArrayEspectadores(espectadores);
		
		
		int filas = 8;
		int columnas = 9;
		String asientos[][] = new String[filas][columnas];		
		rellenarArrayAsientos(asientos);
		
		
		int peliculaEdad= p1.getEdadMinima();
		double precioEntrada = cine.getPrecioEntrada();
		
		for (int i = 0; i < espectadores.length; i++) {
			String nombreEspectador = espectadores[i].getNombre();//Obtenemos el nombre del espectador
			int edadEspectador = espectadores[i].getEdad();//Obtenemos la edad del espectador
			double dineroEspectador = espectadores[i].getDinero();//Obtenemos cuanto dinero tiene el espectador
			//Generamos numeros random de asientos
			int filaSentarse = r1.nextInt(8);
			int columnaSentarse = r1.nextInt(9);
						
			if(edadEspectador >= peliculaEdad && dineroEspectador >= precioEntrada) {//Si el espectador tiene dinero y edad suficiente entra en el if
				//Comprobamos si el asiento esta libre, si no lo esta volvemos generar otro asiento random para el espectador
				if(asientos[filaSentarse][columnaSentarse] == "libre") {
					asientos[filaSentarse][columnaSentarse] = nombreEspectador;
				}else {
					i--;
				}

			}
						
			
		}
		
		mostrarAsientos(asientos);
		
		
		

	}
	
	//Llenamos el array de espectadores
	public static void rellenarArrayEspectadores(Espectador espectadores[]) {
		Espectador e1 = new Espectador("Daniel", 20, 22.5);
		Espectador e2 = new Espectador("Alargo", 34, 3.5);
		Espectador e3 = new Espectador("Javi", 25, 8.0);
		Espectador e4 = new Espectador("Elena", 34, 10.5);
		Espectador e5 = new Espectador("Gabriel", 25, 8.0);
		Espectador e6 = new Espectador("Jose", 14, 5.5);
		Espectador e7 = new Espectador("Andrea", 25, 8.0);
		Espectador e8 = new Espectador("Armando", 50, 60.5);
		Espectador e9 = new Espectador("Andreu", 27, 1.0);
		Espectador e10 = new Espectador("Adrian", 40, 35.5);
		Espectador e11 = new Espectador("Paula", 15, 22.0);
		Espectador e12 = new Espectador("Milena", 21, 150.5);
		Espectador e13 = new Espectador("Alejandro", 19, 37.0);
		
		espectadores[0] = e1;
		espectadores[1] = e2;
		espectadores[2] = e3;
		espectadores[3] = e4;
		espectadores[4] = e5;
		espectadores[5] = e6;
		espectadores[6] = e7;
		espectadores[7] = e8;
		espectadores[8] = e9;
		espectadores[9] = e10;
		espectadores[10] = e11;
		espectadores[11] = e12;
		espectadores[12] = e13;
	}
	
	//Llenamos el array con todos los asientos libres
	public static void rellenarArrayAsientos(String asientos[][]) {
		for (int i = 0; i < asientos.length; i++) {
			for (int j = 0; j < asientos.length; j++) {
				asientos[i][j] = "libre";
			}
		}
	}
	
	//Mostramos el resultado final de como estar�an los asientos
	public static void mostrarAsientos(String asientos[][]) {
		for (int i = 0; i < asientos.length; i++) {
			for (int j = 0; j < asientos.length; j++) {
				System.out.print(asientos[i][j] +"   ");
			}
			System.out.println();
			
		}
	}

}
