package dto;

public class Cine {
	
	protected String peliculaReproduciendo;
	protected double precioEntrada;
	
	
	public Cine(String peliculaReproduciendo, double precioEntrada) {
		this.peliculaReproduciendo = peliculaReproduciendo;
		this.precioEntrada = precioEntrada;
	}


	public String getPeliculaReproduciendo() {
		return peliculaReproduciendo;
	}


	public double getPrecioEntrada() {
		return precioEntrada;
	}
	
	
	
}
