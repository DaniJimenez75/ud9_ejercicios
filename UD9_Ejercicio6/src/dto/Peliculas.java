package dto;

public class Peliculas {
	
	protected String titulo;
	protected int duracion;
	protected int edadMinima;
	protected String director;
	
	
	public String getTitulo() {
		return titulo;
	}
	public int getDuracion() {
		return duracion;
	}
	public int getEdadMinima() {
		return edadMinima;
	}
	public String getDirector() {
		return director;
	}
	public Peliculas(String titulo, int duracion, int edadMinima, String director) {
		this.titulo = titulo;
		this.duracion = duracion;
		this.edadMinima = edadMinima;
		this.director = director;
	}
	
	
	
	

}
